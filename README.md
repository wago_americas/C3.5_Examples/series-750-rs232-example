# PFC200 RS232 via COM1 to a 750-650

## Project Details

- [ ] Written in Structured Text
- [ ] Codesys Version 3.5.19.20
- [ ] Target: PFC200 750-8216 FW26

## Name
PFC200 RS232 via COM1 to a 750-650

## Description
This project demonstrates RS232 communications using a PFC200 processor.
Hardware for this demo has onboard RS232 cabled to 750-650 module.
	
A simple project that sends the alphabet (lower case) from a 750-650 module,
and sends the alphabet (upper case) via the onboard RS232 (DB9) port.
If the module and DB9 port are cabled together, the received data will appear
in the associated input buffer for each device.

![Program](RS232_Comms_Project.png){width=500 height=300}

## Support
This program is for demonstration only and does not include support.  May contain bugs.  Use at your own risk.

## Authors and acknowledgment
Mark DeCramer
May 6 2024

## License
Copyright (c) 2024, Mark DeCramer, WAGO

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE


